var express = require('express');
var app = express();
var port = 3000;
// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
    res.json({
        ok: true,
        msg: 'Todo salió bien'
    });
});
app.listen(port, function () {
    console.log("Example app listening at http://localhost:".concat(port));
});
